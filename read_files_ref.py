def example1_textfile(text_file):
    """
    Uses open/close style of input and assumes you know
    how many lines are in the header
    """

    open_text_file = open(text_file, "r")

    # Skip over the number of header lines
    for i in range(0,59):
        open_text_file.readline()
        i += 1
    # Get the data header from known line at the end of the rest of the
    # header information
    header = open_text_file.readline()

    # Set up empty lists to add to
    year = []
    month = []
    day = []
    cycle = []
    trend = []

    # Read the rest of the data lines into a list in one fell swoop
    data_lines = open_text_file.readlines()

    # Loop over the data lines list to parse information out of each line
    for line in data_lines:
        # Parse the line into a list, split by whitespace
        line_component_list = line.split()
        # Build variable lists from line, changing strings to appropriate data types
        year.append(int(line_component_list[0]))
        month.append(int(line_component_list[1]))
        day.append(int(line_component_list[2]))
        cycle.append(float(line_component_list[3]))
        trend.append(float(line_component_list[4]))

    open_text_file.close()
    
    return year, month, day, cycle, trend

def example2_textfile(text_file):
    """
    Uses `with open` style of input and assumes you know
    what character comment/header lines start with
    """

    with open(text_file, "r") as f:

        data_lines = f.readlines()
            
    # Set up empty lists to add to
    year = []
    month = []
    day = []
    cycle = []
    trend = []
    
    for line in data_lines:
        if line.startswith("#"):
            continue
        else:
            # Parse the line into a list, split by whitespace
            line_component_list = line.split()
            # Build variable lists from line, changing strings to appropriate data types
            year.append(int(line_component_list[0]))
            month.append(int(line_component_list[1]))
            day.append(int(line_component_list[2]))
            cycle.append(float(line_component_list[3]))
            trend.append(float(line_component_list[4]))
            
    return year, month, day, cycle, trend
    
def example3_csvfile(csv_file):
    """
    Uses `with open` style of input, assumes knowledge of 
    header starting character, and handles comma separation
    of values and removal of extraneous whitespace
    """
    
    with open(csv_file, "r") as f:

        data_lines = f.readlines()
            
    # Set up empty lists to add to
    year = []
    month = []
    day = []
    cycle = []
    trend = []
    
    for line in data_lines:
        if line.startswith("#"):
            continue
        elif line.startswith("year"):
            # Need to skip over the section headings too
            continue
        else:
            # Parse the line into a list, split by comma
            line_component_list = line.split(",")
            # Since whitespace isn't the delimter anymore, there may be extra whitespace
            # in the components so strip them as they go into the variable lists
            year.append(int(line_component_list[0].strip()))
            month.append(int(line_component_list[1].strip()))
            day.append(int(line_component_list[2].strip()))
            cycle.append(float(line_component_list[3].strip()))
            trend.append(float(line_component_list[4].strip()))
            
    return year, month, day, cycle, trend

def example4_explore_csvfile(csv_file, header_char, delim_char, subset_records_count):
    """
    Uses `with open` style of input, assumes knowledge of 
    header starting character, and handles delimiter separation
    of values and removal of extraneous whitespace.
    Assumes you do not know variable names or file length.
    Provides access to header content, variable names, 
    and a subset of data without reading the whole file into memory.
    """
    # Set up empty lists to add to
    header = []
    subset_records = []

    with open(csv_file, "r") as f:
        # Read the file line by line because the file length is unknown.
        # Note that readlines() would read the whole file into memory.
        for line in f:
            if len(subset_records) > subset_records_count:
                break
            # If the line starts with the specified special character,
            # assign it as header content
            if line.startswith(header_char):
                # Remove whitespace and assign to header list
                header.append(line.strip())
            else:
                # Remove whitespace and assign to subset records list
                subset_records.append(line.strip())

        # the field names are the first valid row, so take that off the top of the list
        # and split into columns
        fieldname_line = subset_records.pop(0)
        field_names = [ field_name.strip() for field_name in fieldname_line.split(delim_char)]

    return header, field_names, subset_records

if __name__ == "__main__":

    # Swap out example functions and add print statements to see what's happening
    
    # year, month, day, cycle, trend = example1_textfile("co2_trend_gl.txt")
    # year, month, day, cycle, trend = example2_textfile("co2_trend_gl.txt")
    # year, month, day, cycle, trend = example3_csvfile("co2_trend_gl.csv")
    header, field_names, subset_records = example4_explore_csvfile("co2_trend_gl.csv", header_char='#', delim_char=',', subset_records_count=5)